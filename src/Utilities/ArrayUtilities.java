/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.util.Arrays;

/**
 *
 * @author Jbywaters
 */
public class ArrayUtilities {
    public static byte[] extractBlock(byte[] byteData,int blockSize,int blockNum){
        int max = Math.min((blockNum+1)*blockSize,byteData.length);
        return Arrays.copyOfRange(byteData,(blockNum)*blockSize,max);
    }
    
}
