/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Jbywaters
 */
public class FileIO {

    public static String FileToString(String url) {
        String returning = "";
        try (final BufferedReader br = new BufferedReader(new FileReader(url))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                returning = returning + sCurrentLine;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returning;
    }
    public static String[] FileToStringList(String path) {
        ArrayList<String> output = new ArrayList<String>();
        try (final BufferedReader br = new BufferedReader(new FileReader(path))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                output.add(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] returning = new String[output.size()];
        for (int i = 0; i < returning.length; i++) {
            returning[i] = output.get(i);
        }
        return returning;
    }
    
}
