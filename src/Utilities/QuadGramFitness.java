/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jbywaters
 */
public class QuadGramFitness {
    static Map quadgram = new HashMap(); //quadgram map, quite large    
    public static void buildquadmap() { //building quadmap from a text file, assuming first 5 letters of line are valid, any digits after are numbers
        try (BufferedReader br = new BufferedReader(new FileReader("C:\\english_quadgrams.txt"))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                String quad = sCurrentLine.substring(0, 4);
                String num = sCurrentLine.substring(5, sCurrentLine.length());
                int number = Integer.parseInt(num);
                quadgram.put(quad, number);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
        
            public static double quadvalue(String input){ 
        double quadvalue=0;
        int size = quadgram.size();
        //System.out.println(size);
        double logprob = 0;
        char[] charcheck = input.toCharArray();
        for (int i=0;i<charcheck.length;i++){
            charcheck[i] = Character.toUpperCase(charcheck[i]);
        }
        for (int i=3;i<charcheck.length;i++){
            String quad = ""+charcheck[i-3]+charcheck[i-2]+charcheck[i-1]+charcheck[i];
            //System.out.println(quad);
            if (quadgram.containsKey(quad)){
                logprob=(double)(int)quadgram.get(quad)/size;
                //System.out.println(logprob);
            }
            else
                logprob=0;
            quadvalue = quadvalue+logprob;
        }
        return quadvalue;
            }
    
    
    
                public static double fitness(String url,byte[] key) throws UnsupportedEncodingException{
                byte[] rawData = BaseEncoding.b64ToRaw(FileIO.FileToString(url));
                byte[] convertedData = new byte[rawData.length];
                for (int i=0;i<rawData.length;i++){
                    convertedData[i] = (byte)(rawData[i]^key[i%key.length]);
                }
                String decoded = new String(convertedData, "UTF-8");
                //System.out.println(quadvalue(decoded));
                return quadvalue(decoded);
            }
}
