/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jbywaters
 */
public class LetterFrequencyFitness {
    
    static Map CharFreq = new HashMap();
        public static void buildCharacterFrequencyMap() { //building Character frequency map from a text file
        try (BufferedReader br = new BufferedReader(new FileReader("C:\\Letter_Frequency.txt"))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                char letter = sCurrentLine.charAt(0);
                String num = sCurrentLine.substring(1, sCurrentLine.length());
                double number = Double.parseDouble(num);
                CharFreq.put(letter, number);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        }
        
        public static double CharFrequencyScore(String input){
         double TotalDelta=0;
         int NoCharacterPenalty = 45;
         int oddCharacterPenalty = 500;
         char[] inputCharArray = input.toCharArray();
         double[] frequency = new double[CharFreq.size()+1];
         int characterIndex = 0;
         for (int i=0;i<inputCharArray.length;i++){
            inputCharArray[i] = Character.toUpperCase(inputCharArray[i]);
            char currentChar = inputCharArray[i];
            if (CharFreq.containsKey(currentChar)){
                //System.out.println("Found: "+currentChar);
                boolean found=false;
                for (int c=0;c<=characterIndex;c++){
                    if ((byte)currentChar==(byte)inputCharArray[i]){
                        frequency[c]=frequency[c]+1;
                        found=true;
                        //System.out.println("Good Compare with: "+currentChar);
                    }
                }
                if (found==false){
                        inputCharArray[characterIndex] = currentChar;
                        frequency[characterIndex]=1;
                        characterIndex++;
                    }
                //System.out.println(logprob);
            }
            else{
                TotalDelta=TotalDelta+(double)oddCharacterPenalty;
            }
        }
                     for (int b=0;b<characterIndex;b++){
                frequency[b] = frequency[b]/inputCharArray.length;
            }
            for (int b=0;b<characterIndex-1;b++){
                TotalDelta=TotalDelta+(((double)Math.abs(frequency[b]-(double)CharFreq.get(inputCharArray[b])))*100);
                //System.out.println((double)CharFreq.get(characterCountArray[b]));
                //System.out.println(frequency[b]);
            }
            for (int b=0;b<inputCharArray.length;b++){
                if (inputCharArray[b]==(byte)' ')
                    TotalDelta=TotalDelta+NoCharacterPenalty; 
                //System.out.println(inputCharArray[b]);
            }
            //System.out.println(TotalDelta/input.length());
         return (TotalDelta/input.length());
         }
        
    
}
