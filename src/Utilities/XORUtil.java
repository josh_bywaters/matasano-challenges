/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import static Utilities.ArrayUtilities.extractBlock;
import static Utilities.BaseEncoding.StringtoHexBytes;
import java.io.UnsupportedEncodingException;
import static java.lang.System.arraycopy;

/**
 *
 * @author Jbywaters
 */
public class XORUtil {
    
    public static byte[] ArrayByteXor(byte[] preXor,byte[] key){
        int keyLength = key.length;
        int preXorLength = preXor.length;
        byte[] postXor = new byte[preXorLength];
            int keyCounter = 0;
            for (int i=0;i<preXorLength;i++){
                if (keyCounter==keyLength)
                    keyCounter=0;
            postXor[i] = (byte)(preXor[i]^key[keyCounter]);
            keyCounter++;
                    }
return postXor;
    }
    
        public static byte[] SingleByteXor(byte[] preXor,byte key){
        int preXorLength = preXor.length;
        byte[] postXor = new byte[preXorLength];
            int keyCounter = 0;
            for (int i=0;i<preXorLength;i++){
            postXor[i] = (byte)(preXor[i]^key);
            keyCounter++;
                    }
return postXor;
    }

    public static byte[] XorTest(String input, String compare) throws UnsupportedEncodingException {
        byte[] inputbytes = StringtoHexBytes(input);
        byte[] comparebytes = StringtoHexBytes(compare);
        byte[] xord = ArrayByteXor(inputbytes, comparebytes);
        return xord;
    }
    
    
    
    
    
}
