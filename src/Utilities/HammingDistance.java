/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

/**
 *
 * @author Jbywaters
 */
public class HammingDistance {
        public static int hammingDistance(String string1, String string2){
        char[] string1array = string1.toCharArray();
        char[] string2array = string2.toCharArray();
        byte[] string1bytes = new byte[string1array.length];
        byte[] string2bytes = new byte[string2array.length];
        for (int i=0;i<string1array.length;i++)
            string1bytes[i] = (byte) string1array[i];
        for (int i=0;i<string2array.length;i++)
            string2bytes[i] = (byte) string2array[i];
        if (string2bytes.length!=string1bytes.length)
            System.out.println("This shit ain't gonna be right");
        int bitdifference = 0;
        for (int i=0;i<string2bytes.length;i++){
            byte same = (byte)((string1bytes[i]&string2bytes[i]));
            string1bytes[i] = (byte)(string1bytes[i]-same);
            string2bytes[i] = (byte)(string2bytes[i]-same);
            for (int b=64;b>.5;b=b/2){
                if (string1bytes[i]%b!=string1bytes[i]){
                    bitdifference++;
                    string1bytes[i] = (byte)(string1bytes[i]%b);
                }
            }
            for (int b=64;b>.5;b=b/2){
                if (string2bytes[i]%b!=string2bytes[i]){
                    bitdifference++;
                    string2bytes[i] = (byte)(string2bytes[i]%b);
                }
            }
        }
        return bitdifference;
    }
    
}
