/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import static javax.xml.bind.DatatypeConverter.printBase64Binary;

/**
 *
 * @author Jbywaters
 */
public  class BaseEncoding {
    
public static String toBase64(String input){
int length = input.length();
byte[] base16 = new byte[length/2];
for (int i=0;i<length;i=i+2){
    int test =(Character.digit(input.charAt(i),16));
    base16[i/2] = (byte)((Character.digit(input.charAt(i),16)<<4) + Character.digit(input.charAt(i+1),16));
}
int b64length = length*2/3;
byte[] base64 = new byte[b64length];
int b16index = 0;
int varmagicnumber = 0;
for (int i=0;i<base64.length;i++){
    if (varmagicnumber==1||varmagicnumber==2){
        if (b16index+1<=base16.length){
            if (varmagicnumber ==2){
                base64[i] = (byte)(((base16[b16index])<<28>>>26)+(((base16[b16index+1]))<<24>>>30));
                b16index++;
            }
            else if (varmagicnumber==1){
            base64[i] = (byte)(((base16[b16index])<<30>>>26)+((base16[b16index+1]<<24>>>28)));
            b16index++;
            }
            }
        else{
            if (varmagicnumber==1){
                base64[i] = (byte)(base16[b16index]<<6>>>6);
                //System.out.println("endcase1");
            }
        else if (varmagicnumber==2){
            base64[i] =(byte)(base16[b16index]<<4>>>4);
            //System.out.println("endcase2");
        }
        }
        }
    else if (varmagicnumber==3)
        base64[i] =(byte)((base16[b16index]<<26>>>26));
    else if (varmagicnumber==0)
        base64[i] = (byte)((base16[b16index])>>>2);
    //System.out.println(varmagicnumber+" "+base64[i]);
varmagicnumber++;
        if (varmagicnumber>3)
            varmagicnumber=0;
    }
int bytetoint;
byte[] returned = new byte[base64.length];
//System.out.println(base64.length);
for (int i=0;i<base64.length;i++){
    returned[i] = (byte)base64[i];
    //System.out.println(returned[i]);
}
//System.out.println(returned.length);
String test =printBase64Binary(returned);
return test;
}

    public static byte[] StringtoHexBytes(String toHex) {
        int length = toHex.length();
        byte[] base16 = new byte[length / 2];
        for (int i = 0; i < length; i = i + 2) {
            byte currentchar = (byte) toHex.charAt(i);
            base16[i / 2] = (byte) ((Character.digit(currentchar, 16) << 4) + Character.digit(toHex.charAt(i + 1), 16));
        }
        return base16;
    }

    public static byte[] b64ToRaw(String b64String) {
        byte[] rawData = Base64.decode(b64String);
        return rawData;
    }
    public static String rawToB64(byte[] raw){
        return Base64.encode(raw);
    }
    
    public static String byteArrayToHex(byte[] ByteArray) { //DISCLAIMER - I Didn't make this method, just wanted to steal it to quickly test something, credit to Pointer Null on STACKOVERFLOW
   StringBuilder sb = new StringBuilder(ByteArray.length * 2);
   for(byte b: ByteArray)
      sb.append(String.format("%02x", b & 0xff));
   return sb.toString();
}
}