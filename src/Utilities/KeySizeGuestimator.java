/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import static Utilities.HammingDistance.hammingDistance;

/**
 *
 * @author Jbywaters
 */
public class KeySizeGuestimator {
            public static int[] findKeySizeMult(byte[] byteData, int Candidates){
            int maxKeyLength = 128; //maximum key length to test up to
            double[] normEditDistances = new double[maxKeyLength-2];
            int[] keySizes = new int[maxKeyLength-2];
            for (int i=2;i<maxKeyLength;i++){
                int arrayLength = byteData.length/i;
                byte[][] keySizeStrings = new byte[byteData.length/i][i];
                for (int a=0;a<(byteData.length-(i%byteData.length));a=a+i){
                for (int b=a;b<a+i;b++)
                    keySizeStrings[a/i][b-a] =byteData[b];
                }
                double normEditDistance = 0;
                for (int c=0;c<keySizeStrings.length-1;c++){
                    String string1 = new String(keySizeStrings[c]);
                    String string2 = new String(keySizeStrings[c+1]);
                    normEditDistance = normEditDistance+((double)hammingDistance(string1,string2)/i);
                }
                    normEditDistance = (normEditDistance/(keySizeStrings.length));
                    //System.out.println(keySizeStrings.length);
                    normEditDistances[i-2] = normEditDistance;
                    keySizes[i-2] = i;
                    //System.out.println("Normal Edit Distance: "+normEditDistances[i-2]);
                }
            int[] candidates = new int[Candidates];
            double localMin = 100;
            int localMinIndex = 0;
            double max = 100;
            for (int a=0;a<Candidates;a++){
                localMin=100;
                
            for (int i=0;i<normEditDistances.length;i++){
                if (normEditDistances[i]<localMin){
                    localMin=normEditDistances[i];
                    localMinIndex=keySizes[i];
                    normEditDistances[i] = max;
                }
                }
            candidates[a] = localMinIndex;
                    }
           
            //for (int d=0;d<candidates.length;d++)
            //System.out.println("Candidate "+d+": "+candidates[d]);
                return candidates;
            }
            
             public static int findAESFITNESS(byte[] byteData){
            int maxKeyLength = 128; //maximum key length to test up to
            double[] normEditDistances = new double[maxKeyLength-2];
            int[] keySizes = new int[maxKeyLength-2];
            for (int i=2;i<maxKeyLength;i++){
                int arrayLength = byteData.length/i;
                byte[][] keySizeStrings = new byte[byteData.length/i][i];
                for (int a=0;a<(byteData.length-(i%byteData.length));a=a+i){
                for (int b=a;b<a+i;b++)
                    keySizeStrings[a/i][b-a] =byteData[b];
                }
                double normEditDistance = 0;
                for (int c=0;c<keySizeStrings.length-1;c++){
                    String string1 = new String(keySizeStrings[c]);
                    String string2 = new String(keySizeStrings[c+1]);
                    normEditDistance = normEditDistance+((double)hammingDistance(string1,string2)/i);
                }
                    normEditDistance = (normEditDistance/(keySizeStrings.length));
                    //System.out.println(keySizeStrings.length);
                    normEditDistances[i-2] = normEditDistance;
                    keySizes[i-2] = i;
                    //System.out.println("Normal Edit Distance: "+normEditDistances[i-2]);
                }
            double localMin = 100;
            int localMinIndex = 0;
            double max = 100;
                localMin=100000;
                int candidate;
            for (int i=0;i<normEditDistances.length;i++){
                if (normEditDistances[i]<localMin){
                    localMin=normEditDistances[i];
                    localMinIndex=keySizes[i];
                    normEditDistances[i] = max;
                }
                }
            candidate = localMinIndex;
           
            //for (int d=0;d<candidates.length;d++)
            //System.out.println("Candidate "+d+": "+candidates[d]);
                return candidate;
            }
    
}
