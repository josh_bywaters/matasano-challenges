/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AESManipulation;

import static Utilities.ArrayUtilities.extractBlock;
import static Utilities.XORUtil.ArrayByteXor;
import java.io.UnsupportedEncodingException;
import static java.lang.System.arraycopy;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import static javax.crypto.Cipher.DECRYPT_MODE;
import static javax.crypto.Cipher.ENCRYPT_MODE;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class AESMain {

    public static byte[] AESCipher(Key password, int mode, byte[] input) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        if (mode == 0) //clumsy method of switching between decrypt and encrypt, debating just using two functions, but this is smaller
        {
            mode = ENCRYPT_MODE;
        } else {
            mode = DECRYPT_MODE;
        }
        //System.out.println(decoded);
        Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding"); //initializing cipher to use AES in ECB mode
        cipher.init(mode, password);
        return cipher.doFinal(input); //Decrypting/Encrypting the block of data 'input'
    }

    public static Key makeAESKey(byte[] key) { //Making a key object for using various ciphers -- If I find I need to make more, I'll restructure to accept a string
        SecretKeySpec Returnedkey = new SecretKeySpec(key, "AES"); //for AES
        return Returnedkey;
    }

    /**
     *
     * @param startingBytes - unpadded
     * @param keySize
     * @return
     */
    public static byte[] pkcs7Padding(byte[] startingBytes, int keySize) {
        int startingSize = startingBytes.length;
        int toBePadded = keySize - (startingSize % keySize);
        if (toBePadded == keySize) {
            return startingBytes;
        } else {
            //System.out.println(toBePadded);

            byte[] newBytes = new byte[startingSize + toBePadded];
            System.arraycopy(startingBytes, 0, newBytes, 0, startingSize);
            String padding = "" + toBePadded;
            int startIndex = startingSize;
            if (newBytes.length > startingSize) {
                for (int i = 0; i < toBePadded; i++) {
                    newBytes[i + startIndex] = padding.getBytes()[0];
                }
            }
            return newBytes;
        }
    }

    public static byte[] AESCBCMode(byte[] preWorkBytes, byte[] key, byte[] IV, int mode) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException { //mode corresponds to AESECB method format, 0 is encrypt, anything else is decrypt
        Key CryptoKey = makeAESKey(key);
        byte[] workingBlock;
        byte[] previousBlock = null;
        int keyLength = key.length;
        int workingLength = preWorkBytes.length;
        int currentIndex = 0;
        int blocks = workingLength / keyLength;
        byte[] processed = new byte[workingLength];
        byte[] decrypted;
        for (int currentBlock = 0; currentBlock < blocks; currentBlock++) {
            workingBlock = extractBlock(preWorkBytes, keyLength, currentBlock);
            if (currentBlock == 0) {
                previousBlock = IV;
            }
            if (mode == 0) { //encrypting
                previousBlock = AESCipher(CryptoKey, mode, ArrayByteXor(workingBlock, previousBlock));//gives cyphertext
                decrypted = previousBlock; //for copying purposes
            } else { //decrypting
                decrypted = ArrayByteXor(AESCipher(CryptoKey, mode, workingBlock), previousBlock);//gives plaintext
                previousBlock = workingBlock;
            }
            arraycopy(decrypted, 0, processed, currentIndex, keyLength);
            currentIndex += keyLength;
        }
        return processed;
    }

}
