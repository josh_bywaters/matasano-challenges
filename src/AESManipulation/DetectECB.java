/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AESManipulation;

import static Utilities.ArrayUtilities.extractBlock;
import static Utilities.BaseEncoding.b64ToRaw;
import static Utilities.FileIO.FileToStringList;
import java.util.Arrays;

/**
 *
 * @author Jbywaters
 */
public class DetectECB {

    public static int duplicateBlocks(byte[] ecb) {
        int keysize = 16;
        int dataLength = ecb.length;
        int blocks = ecb.length / keysize;
        int matches = 0;
            for (int CurrentBlock = 0; CurrentBlock < blocks - 1; CurrentBlock++) {
                byte[] baseBlock = extractBlock(ecb, keysize, CurrentBlock);
                for (int nextBlock = CurrentBlock + 1; nextBlock < blocks; nextBlock++) {
                    byte[] compareBlock = extractBlock(ecb, keysize, nextBlock);
                    if (Arrays.equals(baseBlock, compareBlock)) {
                        matches++;
                    }
                }
            }
        
        return matches;
    }

    public static String detectECB(String url) {
        int keysize = 16;
        String[] data = FileToStringList(url);
        byte[][] byteData = new byte[data.length][];
        int[] fitness = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            byteData[i] = b64ToRaw(data[i]);
            fitness[i] = duplicateBlocks(byteData[i]);
        }
        int fitmax = 0;
        int fitmaxindex = 0;
        int fitSecondPlace = 0;
        for (int i = 0; i < fitness.length; i++) {
            if (fitness[i] > fitmax) {
                fitSecondPlace = fitmax;
                fitmax = fitness[i];
                fitmaxindex = i;
            }
        }
        System.out.println("The max fitness is" + fitmax + "And the Runner up is " + fitSecondPlace);
        return data[fitmaxindex];
    }

    public static int appendedECB(byte[] ecb, int minAppend, int maxAppend) {
        int maxRepeatedBlocks = 0;

        int ecbLength = ecb.length;
        for (int append = minAppend; append <= maxAppend; append++) {
            byte[] unappendedECB = new byte[ecbLength - append];
            System.arraycopy(ecb, append, unappendedECB, 0, ecbLength - append);
            int fitness = duplicateBlocks(unappendedECB);
            System.out.println(fitness);
            if (fitness > maxRepeatedBlocks) {
                maxRepeatedBlocks = fitness;
            }
        }
        return maxRepeatedBlocks;
    }

}
