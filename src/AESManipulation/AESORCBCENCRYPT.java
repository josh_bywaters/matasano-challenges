/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AESManipulation;

import static AESManipulation.AESMain.AESCBCMode;
import static AESManipulation.AESMain.AESCipher;
import static AESManipulation.AESMain.makeAESKey;
import static AESManipulation.AESMain.pkcs7Padding;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Joshua
 */
public class AESORCBCENCRYPT {
    public static Key randKey(int keySize){
        Random rand = new Random();
        byte[] key = new byte[keySize];
        rand.nextBytes(key);
        Key returnKey = makeAESKey(key);
        return (returnKey);
    }
    public static byte[] RandEncryptAES(byte[] plaintext,int keySize) throws NoSuchAlgorithmException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException{
        Key key = randKey(keySize);
        Random rand = new Random();
        int plaintextLength = plaintext.length;
        int appendmax =10;
        int appendmin =5;
        int preAppend = rand.nextInt((appendmax-appendmin)+1)+appendmin;
        int postAppend = rand.nextInt((appendmax-appendmin)+1)+appendmin;
        byte[] appendedBytes = new byte[plaintextLength+preAppend+postAppend];
        byte[] firstAppend = new byte[preAppend];
        byte[] lastAppend = new byte[postAppend];
        System.out.println(preAppend+" Bytes appended to start");
        rand.nextBytes(firstAppend);
        rand.nextBytes(lastAppend);
        System.arraycopy(firstAppend, 0, appendedBytes, 0,preAppend);
        System.arraycopy(plaintext,0,appendedBytes,preAppend,plaintextLength);
        System.arraycopy(lastAppend,0,appendedBytes,plaintextLength+preAppend,postAppend);
        //System.out.println(appendedBytes.length);
        //System.out.println(pkcs7Padding(appendedBytes,keySize).length);
        if (rand.nextBoolean()){
            System.out.println("ECB");
            return AESCipher(key,0,pkcs7Padding(plaintext,keySize));
            
        }
        else{
            byte[] IV = new byte[keySize];
            rand.nextBytes(IV);
            //System.out.println(key.getEncoded().length);
            System.out.println("CBC");
            return AESCBCMode(pkcs7Padding(appendedBytes,keySize),key.getEncoded(),IV,0);
        }
        
            
        
    
}
}
