/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VariablebyteXOREncryption;

import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Jbywaters
 */
public class VarByteXorEncrypt {
    public static String VarByteXorEncrypt(String initialString, String encryptBytes){
        int bytenum = 0;
        byte[] encrypt = new byte[encryptBytes.length()];
        for (int i=0;i<encryptBytes.length();i++){
            encrypt[i] = (byte) encryptBytes.charAt(i);
            System.out.println(encrypt[i]);
        }
        byte[] basebits = new byte[initialString.length()];
        for (int i=0;i<basebits.length;i++)
            basebits[i] = (byte)(initialString.charAt(i));
        
        for (int i=0;i<basebits.length;i++){
            basebits[i] = (byte)(basebits[i]^encrypt[bytenum]);
            bytenum++;
            if (bytenum>=encrypt.length)
                bytenum=0;
        }
        return DatatypeConverter.printHexBinary(basebits);
    }
}
