/*
Repeating Xor decryption program that guestimates a set of keylengths, breaks the given encrypted data down into strings of similar-encrypted bits
And then decrypts each similar bit string, placing all of them back together, and pitting each guestimated keylength against each other in fitness
to the english language. I had to swap from a purely quadgram-based fitness measurement to a character frequency fitness test, because the data--
would not be decrypted in order, it would be jarbled and the quadgrams would be ineffective. I ended up using both, the quadgram fitness check is
used to check each keylength's results, as I think that may be a better measurement for actual english words. Either fitness test seems to work great
for plaintext english.
 */
package BreakingRepeatingKeyXor;

import Utilities.BaseEncoding;
import Utilities.FileIO;
import static Utilities.KeySizeGuestimator.findKeySizeMult;
import static Utilities.LetterFrequencyFitness.CharFrequencyScore;
import static Utilities.QuadGramFitness.fitness;
import java.io.UnsupportedEncodingException;
/**
 *
 * @author Jbywaters
 */
public class RepeatingXor {

//System.out.println(CharFreq.toString());
            public static String xortospecific(String input, byte compare) throws UnsupportedEncodingException { //converts a hex string to a byte array that has been xor'd with the given compare
        byte[] stringtoBytes = input.getBytes();
        byte[] byteresult = new byte[stringtoBytes.length];
        for (int i = 0; i < stringtoBytes.length; i++) {
            byteresult[i] = (byte) (stringtoBytes[i] ^ compare);
        }
        String decoded = new String(byteresult, "UTF-8");
        return decoded;
    }
            public static byte XorDecrypttobyte(String initialString) throws UnsupportedEncodingException { //gives bytes to the xortospecific function, then rates the results with the ratetheshit64bstyle, then gives the best string
        double[] ratings = new double[256];
        for (int i = 0; i < 255; i++) {
            ratings[i] = CharFrequencyScore(xortospecific(initialString, (byte) i));
        }
        double min=1000000;
        byte BestFitByte = 0;
        for (int i = 0; i < ratings.length; i++) {
            //System.out.println(ratings[i]);
            if (ratings[i] < min&&ratings[i]!=0) {
                min = ratings[i];
                BestFitByte =(byte) i;
                //System.out.println(min+" IS the minimum"+i);
            }
        }
        //System.out.println(min+" Rating");
        return  BestFitByte;
    }
            

            
            public static String decrypt(String url,byte[] key) throws UnsupportedEncodingException{
                byte[] rawData = BaseEncoding.b64ToRaw(FileIO.FileToString(url));
                byte[] convertedData = new byte[rawData.length];
                for (int i=0;i<rawData.length;i++){
                    convertedData[i] = (byte)(rawData[i]^key[i%key.length]);
                }
                String decoded = new String(convertedData, "UTF-8");
                return decoded;
            }
            
            public static String decrypt2(String EncryptedHex,byte[] key) throws UnsupportedEncodingException{
        int ilength = EncryptedHex.length();
        byte[] input16 = new byte[(ilength / 2)+ilength%2];
        for (int i = 0; i < ilength; i = i + 2) {
            if (i+1<ilength)
            input16[i / 2] = (byte) ((Character.digit(EncryptedHex.charAt(i), 16) << 4) + Character.digit(EncryptedHex.charAt(i + 1), 16));
            else
            input16[i / 2] = (byte) (Character.digit(EncryptedHex.charAt(i), 16));
        }
        byte[] byteresult = new byte[input16.length];
        for (int i = 0; i < input16.length; i++) {
            byteresult[i] = (byte) (input16[i]^key[i%key.length]);
        }
        String decoded = new String(byteresult, "UTF-8");
        return decoded;
            }
            
            public static byte[] fitnessComparison(String url,byte[][] keys) throws UnsupportedEncodingException{
                double maxFitness = 0;
                int maxKeyIndex = 0;
                for (int i=0;i<keys.length;i++){
                    if (fitness(url,keys[i])>maxFitness){
                        maxFitness=fitness(url,keys[i]);
                        maxKeyIndex=0;
                    }
                }
                return keys[maxKeyIndex];
            }
            
            public static byte[] loadKeyRing(String url) throws UnsupportedEncodingException{ //Does most of the end logic for the decryption
                
            int[] candidates = findKeySizeMult(BaseEncoding.b64ToRaw(FileIO.FileToString(url)), 1); //candidates from the keysize calculator
            //    int[] candidates = new int[40];
            //for (int i=1;i<=candidates.length;i++){ //used to troubleshoot what the problem was, found out my keysize calculator was wrong
            //   candidates[i-1] = i;
           // }
            byte[] rawData = BaseEncoding.b64ToRaw(FileIO.FileToString(url)); //raw encrypted Data
            byte[][] BestKey = new byte[1][]; //Keeping track of best key
            double BestFitness = 0; //keeping track of best fitness out of all candidates
            for (int i=0;i<candidates.length;i++){ //iterates through each candidate keylength, running logic on each and eventually checking each's fitness against each other
                byte[] key = new byte[candidates[i]]; //key of length given in candidates
                String[] sameBitStrings = new String[candidates[i]]; //String that will be used to put together all bytes that correspond to each of the keys bytes
                for (int b=0;b<rawData.length;b++){ //iterates through the data, filling the strings with like-encoded bits, for later decryption
                    int index = (b%candidates[i]); 
                    sameBitStrings[index] = sameBitStrings[index]+(char)rawData[b]; //making the strings
                }
                    for (int c=0;c<sameBitStrings.length;c++){ //sending each string through a decryptor, that gives the best fit for each string of jarbled same-encryption byte key bytes
                    key[c] = XorDecrypttobyte(sameBitStrings[c]);
                }
                    if (fitness(url,key)>BestFitness){ //finds best fitness of each of the keys, to find the true last key. Uses the quadgram fitness checker
                        BestKey[0]=key;
                        BestFitness=fitness(url,key);
                    }
            }
            return BestKey[0];
            }
            
        public static String mainish(String url) throws UnsupportedEncodingException{ //last method, to call the encryption
        byte[] chosenKey = loadKeyRing(url);
        
        return decrypt(url,chosenKey);
        }
                
        }