/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static AESManipulation.AESMain.*;
import static AESManipulation.DetectECB.*;
import static BreakingRepeatingKeyXor.RepeatingXor.decrypt2;
import static BreakingRepeatingKeyXor.RepeatingXor.mainish;
import static Utilities.BaseEncoding.b64ToRaw;
import static Utilities.BaseEncoding.byteArrayToHex;
import static Utilities.BaseEncoding.toBase64;
import static Utilities.FileIO.FileToString;
import static Utilities.FileIO.FileToStringList;
import static Utilities.HammingDistance.hammingDistance;
import static Utilities.LetterFrequencyFitness.buildCharacterFrequencyMap;
import static Utilities.QuadGramFitness.buildquadmap;
import Utilities.XORUtil;
import static VariablebyteXOREncryption.VarByteXorEncrypt.VarByteXorEncrypt;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import static xor.cypher.XorCypher.XorDecrypt;
import static xor.cypher.list.XorCypherList.XorCypherList;

/**
 *
 * @author Jbywaters
 */
public class Set1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException {
       // buildquadmap(); //Necessary for checking quadgrams
        //buildCharacterFrequencyMap();
        /* Base 16 to Base 64 Converter
        
        */
        //System.out.println(toBase64("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"));
        
        /* Fixed size Xor compare
        REQUIRES: HEX input string, and equal HEX compare string
        */
        //System.out.println(byteArrayToHex(XORUtil.XorTest("1c0111001f010100061a024b53535009181c","686974207468652062756c6c277320657965")));
        /* Single Byte Xor Decrypt
        REQUIRES: HEX input string. Will move through all 256 combinations of a byte, and xor's the byte against every byte in input string
        */
        
        //System.out.println(XorDecrypt("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")+" Is my estimated string");
        
       /*Single Byte Xor Decrypt - List version
        REQUIRES: list populated with strings to be decrypted via a single byte XOR decryption
        RETURNS: Best Fit after finding best fitness of each string's decrypt, then checking fitness of each best fitness.
        */
        //System.out.println("Best fit out of the list Is: "+XorCypherList("C:\\4.txt"));
        
        /*Variable Byte Xor Encryption
        REQUIRES: (1) Initial String to be encrypted in HEX format
                  (2) List of Characters to convert to bytes, and be used to encrypt (1)
        RETURNS: Encrypted (1) String
        */
        //System.out.println(VarByteXorEncrypt("Burning 'em, if you ain't quick and nimble I go crazy when I hear a cymbal","ICE"));
        
        /*Breaking Variable Byte Xor Encryption
        REQUIRES: (1) Initial String to be broken in 64 bit format
        RETURNS: Guessed decryption of encrypted input
        */
        //System.out.println(hammingDistance("this is a test","wokka wokka!!!"));
        //System.out.println(mainish("C:\\6.txt"));
        
        //System.out.println(decrypt2(VarByteXorEncrypt("Burning 'em, if you ain't quick and nimble I go crazy when I hear a cymbal","ICE"),"ICE".getBytes()));
        
        //Exercise 7
        //byte[] bytePass = "YELLOW SUBMARINE".getBytes();
        //SecretKey key = new SecretKeySpec(bytePass,0,bytePass.length,"AES");
        //String decoded = new String(AESCipher(makeAESKey(bytePass),1,b64ToRaw(FileToString("C:\\7.txt"))), "UTF-8");
        //System.out.println(decoded);
        
        //Exercise 8
        System.out.println(detectECB("C:\\8.txt"));
       
        

       
    }
}
