/*Josh Bywaters 1/1/2015
xorCypher single byte key finder
Includes a quadgram "english probability" method
 */
package xor.cypher;

import static Utilities.LetterFrequencyFitness.CharFrequencyScore;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Jbywaters
 */
public class XorCypher {

    

    public static String HexXorDecode(String input, byte compare) throws UnsupportedEncodingException { //converts a hex string to a byte array that has been xor'd with the given compare
        int ilength = input.length();
        byte[] input16 = new byte[(ilength / 2)+ilength%2];
        for (int i = 0; i < ilength; i = i + 2) {
            if (i+1<ilength)
            input16[i / 2] = (byte) ((Character.digit(input.charAt(i), 16) << 4) + Character.digit(input.charAt(i + 1), 16));
            else
            input16[i / 2] = (byte) (Character.digit(input.charAt(i), 16));
        }
        byte[] byteresult = new byte[input16.length];
        for (int i = 0; i < input16.length; i++) {
            byteresult[i] = (byte) (input16[i] ^ compare);
        }
        String decoded = new String(byteresult, "UTF-8");
        return decoded;
    }
    
    public static String XorDecrypt(String initialString) throws UnsupportedEncodingException { //gives bytes to the HexXorDecode function, then rates the results with the ratetheshit64bstyle, then gives the best string
        double[] ratings = new double[256];
        for (int i = 0; i < 256; i++) {
            ratings[i] = CharFrequencyScore(HexXorDecode(initialString, (byte) i));
        }
        double max = 20000;
        byte maxindex = 0;
        for (int i = 0; i < ratings.length; i++) {
            if (ratings[i] < max) {
                max = ratings[i];
                maxindex = (byte)i;
            }
        }
        return HexXorDecode(initialString,  maxindex);
    }


    /*
    detecting quadvalue in a double format, checks every 4 letters of a string against the quadgram map, adding probability
    to an eventual value, and keeping track of a max index value for later use
    */

        //System.out.println(input+"'s value is "+quadvalue); //use this if you want to see every string that gets checked, as well as the quadvalue of that string
}
