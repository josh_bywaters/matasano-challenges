/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xor.cypher.list;

import Utilities.FileIO;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import static xor.cypher.XorCypher.XorDecrypt;
import static Utilities.QuadGramFitness.quadvalue;

/**
 *
 * @author Jbywaters
 */
public class XorCypherList {
    public  static String CheckEach (String[] encryptedList) throws UnsupportedEncodingException{
        int BestFit = 0;
        double max = 0.0;
        String[] possibleResults = new String[encryptedList.length];
        for (int i=0;i<encryptedList.length;i++){
            possibleResults[i] = XorDecrypt(encryptedList[i]);
            //System.out.println(possibleResults[i]);
        }
        for (int i=0;i<possibleResults.length;i++){
            double quadvalue = quadvalue(possibleResults[i]);
            if (quadvalue>max){
                max=quadvalue;
                BestFit = i;
            }
        }
        return XorDecrypt(encryptedList[BestFit]);
        
    }
    public static String XorCypherList(String path) throws UnsupportedEncodingException{
        return CheckEach(FileIO.FileToStringList(path));
    }

}
